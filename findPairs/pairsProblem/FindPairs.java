/* Ben Nathanson
*  15 July 2018
*
* This program is designed to explore a problem where given a random sequence
* of integers, we want to find all the pairs of numbers in the sequence that have
* a difference of 2. I have generalized the solution to take in lower bound,
* upperbound, sequence length, and difference arguments.
*
* To run the basic problem, one would compile and run FindPairs with
* something along the lines of "java pairsProblem.FindPairs 0 20 10 2".
*
* It occured to me that this can crudely demonstrate the dynamics of the classic
* birthday problem in discrete mathematics.
* Simply run "java pairsProblem.FindPairs 0 365 x 0", change x according to your
* best guess of how many people would need to be in a room for at least two people 
* to have the same birthday.
*
* My solution still runs two for loops, but it should beat n^2 runtime on average
* because unlike a naive solution where we compare all the n numbers with each other,
* my solution cuts down on comparisons where we know the result will be false.
* Still, if we were to run this where the upperbound - lowerbound = difference,
* my algorithm would have to compare every element with every other element.
*/
package pairsProblem;
import java.util.Arrays; // used to sort an array

public class FindPairs {
  private static String pairsList = "No pairs found. "; // message if nothing is found
  private static int countPairs;
  // find pairs
  public static void findPairs (int[] toBeSearched, int gap) {
  countPairs = 0; // keep track of how many pairs have been found
  String pairsFound = ""; // string to build output
  int firstNum, secondNum; // saves a little time in the for loop
  int arrLength = toBeSearched.length; // length of array
  Arrays.sort(toBeSearched); // sort the array
  // sorting is critical to the runtime of this algorithm as it cuts down
  // on needless comparisons between elements
  for (int i = 0; i < arrLength; i++) {
    firstNum = toBeSearched[i];
    for (int j = (i + 1); ((j < arrLength) && (!((toBeSearched[j] - firstNum) > (gap)))); j++) {
      secondNum = toBeSearched[j];
        if ((secondNum - firstNum) == gap) {
          countPairs++;
          j++;
          if (countPairs > 1) {
            pairsFound += ", ";
          } // end if
          pairsFound += "(" + Integer.toString(firstNum) +  ","
                      + Integer.toString(secondNum) + ")";
        } // end if
    } // end for
  } // end for
  if (countPairs > 0) {
    pairsList = pairsFound; // if we found pairs, change the output to the pairs found
    // otherwise, the main method prints "No pairs found."
  } // end if
} // end find pairs

  public static void main (String[] args) {
    if(args.length == 4) {
      int lowBound = Integer.parseInt(args[0]);
      int upperBound = Integer.parseInt(args[1]);
      int arrayLength = Integer.parseInt(args[2]);
      int gapBetweenNums = Integer.parseInt(args[3]);
      int[] outputArray = new int[arrayLength];
      ArrayBuilder buildArray = new ArrayBuilder();
      outputArray = buildArray.ArrayBuilder(lowBound, upperBound, arrayLength, false);
      System.out.println("Working with the sequence: " + Arrays.toString(outputArray));
      findPairs(outputArray, gapBetweenNums);
      System.out.println("We found " + Integer.toString(countPairs) + " pairs: " + pairsList);
    } // end if
    else {
      System.out.println("Insufficent arguments. FindPairs expects 3 ints "
      + "to generate its sequence of numbers: a lower bound, an upper bound "
      + " and a length argument. e.g. 'FindPairs 0 10 5' would generate an sequence "
      + "like [1, 5, 2, 3, 9].");
    } // end else
} //end main
} // end FindPairs
