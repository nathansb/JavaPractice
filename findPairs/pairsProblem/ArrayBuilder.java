// Ben Nathanson
package pairsProblem;
import java.util.Arrays; // sorting an array
import java.util.Random; // creating random numbers
// this method returns an integer array of length length with numbers ranging
// from low to high, and may be sorted (ascending) or unsorted
public class ArrayBuilder{
  private int low = 0, high = 0, length = 0;
  private boolean sort = false;
  public static int[] ArrayBuilder(int low, int high, int length, boolean sort) {
    int[] output = new int[length]; // initialize the array
    Random rand = new Random(); // initialize the random number generator
    for (int i = 0; i < length; i++) {
      output[i] = rand.nextInt((high - low) + 1) + low; //populate the array with random numbers
    }
    if (sort) {
      Arrays.sort(output); // if sort == true, sort the array ascending
    }
    return output; // return the array
  } // end arrayBuilder
/*
  public static void main(String[] args) {
    if(args.length == 4) {
      int lowBound = Integer.parseInt(args[0]);
      int upperBound = Integer.parseInt(args[1]);
      int arrayLength = Integer.parseInt(args[2]);
      boolean returnSorted = Boolean.parseBoolean(args[3]);
      int[] array = new int[arrayLength];
      array = arrayBuilder(lowBound, upperBound, arrayLength, returnSorted);
      System.out.println(Arrays.toString(array));
    } // end if
    else {
      System.out.println("Insufficent arguments. ArrayBuilder expects 4.");
    } // end else

  }
  */
}
